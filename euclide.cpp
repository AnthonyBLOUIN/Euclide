/*-----------------------------------
	author : anthony BLOUIN
-------------------------------------*/

#include <iostream>
using namespace std;

int main()
{
	int i(0),a, b, r;


	// user input
	cout<<"nombre A : ";
	cin>>a;
	cout<<"nombre B : ";
	cin>>b;

	// calcul loop
	do
	{
		i++
		r = a%b;
		cout<<"etape "<<i<<"\t|"<<a<<" = "<<b<<" x "<<a/b<<" + "<<r<<"\t\t|"<<a<<" | "<<b<<" | "<<r<<endl;
		a = b;
		b = r;
	}while(r != 0);

	return 0;
}

